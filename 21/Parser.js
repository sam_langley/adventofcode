const fs = require('fs');


class Parser {
    constructor(commandFile) {
        this.fileContents = fs.readFileSync(commandFile, 'utf-8');
        this.lines = this.fileContents.split('\n').reduce((prev, current, index) => current.length > 0 ? [...prev, current] : prev, [])
    }

    getCommands() {
        return this.lines.map(command => {
            let chunks = command.replace(/[x|y|z]=/g, '').split(' ');
            const coords = chunks[1].split(',');
            return ({
                toggle: chunks[0],
                x: coords[0].split('..').map(coord => parseInt(coord)),
                y: coords[1].split('..').map(coord => parseInt(coord)),
                z: coords[2].split('..').map(coord => parseInt(coord)),
            })
        })
    }

    getCommandCount() {
        return this.lines.length
    }
}

module.exports = Parser;
