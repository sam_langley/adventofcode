class Reactor {
    constructor(xDims, yDims, zDims) {
        this.xMin = xDims[0];
        this.xMax = xDims[1];
        this.yMin = yDims[0];
        this.yMax = yDims[1];
        this.zMin = zDims[0];
        this.zMax = zDims[1];

        this.grid = [];
        for (let i = 0; i <= this.xMax - this.xMin; i++) {
            if (!this.grid[i]) this.grid[i] = [];
            for (let j = 0; j <= this.yMax - this.yMin; j++) {
                if (!this.grid[i][j]) this.grid[i][j] = [];
                for (let k = 0; k <= this.zMax - this.zMin; k++) {
                    this.grid[i][j][k] = 0;
                }
            }
        }
    }

    volume() {
        return (this.xMax - this.xMin) *
            (this.yMax - this.yMin) *
            (this.zMax - this.zMin);
    }

    applyCommands(commands) {
        for (let command of commands) {
            if (command.toggle === 'on') {
                this.setRegionOn(command.x, command.y, command.z)
            } else {
                this.setRegionOff(command.x, command.y, command.z)
            }
        }
    }

    setRegionOn(xDims, yDims, zDims) {
        this.__setRegionToValue(xDims, yDims, zDims, 1);
    }

    setRegionOff(xDims, yDims, zDims) {
        this.__setRegionToValue(xDims, yDims, zDims, 0);
    }

    getCountOfCubesOn() {
        let count = 0;

        for (let i = 0; i <= this.xMax - this.xMin; i++) {
            for (let j = 0; j <= this.yMax - this.yMin; j++) {
                for (let k = 0; k <= this.zMax - this.zMin; k++) {
                    if (this.grid[i][j][k] !== 0) count++;
                }
            }
        }

        return count;
    }

    __setRegionToValue(xDims, yDims, zDims, value) {
        for (let i = Math.max(xDims[0], -50) + 50; i <= Math.min(xDims[1], 50) + 50; i++) {
            for (let j = Math.max(yDims[0], -50) + 50; j <= Math.min(yDims[1], 50) + 50; j++) {
                for (let k = Math.max(zDims[0], -50) + 50; k <= Math.min(zDims[1], 50) + 50; k++) {
                    this.grid[i][j][k] = value;
                }
            }
        }
    }
}

module.exports = Reactor;
