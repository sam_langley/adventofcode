const Parser = require('./Parser.js');
const Reactor = require('./Reactor.js');


const parser = new Parser('./complex.txt');
const reactor = new Reactor([-50, 50], [-50, 50], [-50, 50]);
reactor.applyCommands(parser.getCommands());
console.log(reactor.getCountOfCubesOn())
