const { expect } = require('@jest/globals');
const Parser = require('../Parser.js');

test('parser can understand a simple file', () => {
    const parser = new Parser('./simple.txt');
    expect(parser.getCommandCount()).toEqual(4);
    expect(parser.getCommands()).toEqual([
        { "toggle": "on", "x": [10, 12], "y": [10, 12], "z": [10, 12] },
        { "toggle": "on", "x": [11, 13], "y": [11, 13], "z": [11, 13] },
        { "toggle": "off", "x": [9, 11], "y": [9, 11], "z": [9, 11] },
        { "toggle": "on", "x": [10, 10], "y": [10, 10], "z": [10, 10] },
    ]);
});

test('parser can understand a file without repeating tuples', () => {
    const parser = new Parser('./simple2.txt');
    expect(parser.getCommands()).toEqual([
        { "toggle": "on", "x": [-20, 10], "y": [10, 22], "z": [10, 13] },
    ]);
});

test('parser can understand a complex file', () => {
    const parser = new Parser('./complex.txt');
    expect(parser.getCommandCount()).toEqual(420);
});
