const { expect } = require('@jest/globals');
const Reactor = require('../Reactor.js');
const Parser = require('../Parser.js');

test('reactor can return the volume', () => {
    const reactor = new Reactor([-50, 50], [-50, 50], [-50, 50],);
    expect(reactor.volume()).toEqual(1000000)
});

test('a single region of the reactor can be turned on or off', () => {
    const reactor = new Reactor([-50, 50], [-50, 50], [-50, 50]);
    reactor.setRegionOn([10, 12], [10, 12], [10, 12])
    expect(reactor.getCountOfCubesOn()).toEqual(27)
});

test('multiple regions of the reactor can be turned on or off', () => {
    const reactor = new Reactor([-50, 50], [-50, 50], [-50, 50]);
    reactor.setRegionOn([10, 12], [10, 12], [10, 12])
    reactor.setRegionOn([11, 13], [11, 13], [11, 13])
    reactor.setRegionOff([9, 11], [9, 11], [9, 11])
    reactor.setRegionOn([10, 10], [10, 10], [10, 10])
    expect(reactor.getCountOfCubesOn()).toEqual(39)
});

test('nothing happens if we toggle a region outside the reactor', () => {
    const reactor = new Reactor([-50, 50], [-50, 50], [-50, 50]);
    reactor.setRegionOn([-5010, -100], [10, 12], [10, 12])
    expect(reactor.getCountOfCubesOn()).toEqual(0)
});

test('it can cope with a region that is massive and also intersects the cube', () => {
    const reactor = new Reactor([-50, 50], [-50, 50], [-50, 50]);
    reactor.setRegionOn([-5010, 0], [10, 10], [10, 10])
    expect(reactor.getCountOfCubesOn()).toEqual(51)
});

test('it can handle commands being passed in', () => {
    const reactor = new Reactor([-50, 50], [-50, 50], [-50, 50]);
    const parser = new Parser('./medium.txt');
    reactor.applyCommands(parser.getCommands())
    expect(reactor.getCountOfCubesOn()).toEqual(590784)
});
